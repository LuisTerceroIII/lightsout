package utils;

public class Utils {

    public static int getRandomBetween(int min, int max) {
        return (int) Math.floor(Math.random() * (max - min + 1) + min);
    }
}

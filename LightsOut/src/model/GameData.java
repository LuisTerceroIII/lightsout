package model;

import java.util.ArrayList;

public class GameData {
	private Integer[] levels;
	private int movements;
	private final ArrayList<ObserverGameData> observers = new ArrayList<>();

	public GameData(Integer[] levels) {
		this.levels = levels;
		this.movements = 0;
	}

	public Integer[] getLevels() {
		return levels;
	}

	public void setLevels(Integer[] levels) {
		this.levels = levels;
	}

	public int getMovements() {
		return movements;
	}

	public void setMovements(int movements) {
		this.movements = movements;
	}

	public void plusMovement() {
		this.movements++;
		notifyAllMovementsCounterObservers();
	}

	public void resetMovements() {
		setMovements(0);
		notifyAllResetMovementsCounterObservers();
	}

	public void attach(ObserverGameData observer) {
		observers.add(observer);
	}

	public void notifyAllMovementsCounterObservers() {
		for (ObserverGameData observer : observers) {
			observer.updateMovements();
		}
	}

	public void notifyAllResetMovementsCounterObservers() {
		for (ObserverGameData observer : observers) {
			observer.resetMovements();
		}
	}
}

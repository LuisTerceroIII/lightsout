package model;
import utils.Utils;

import java.util.ArrayList;

public class Board {
    private Light[][] lights;
    private int dimension;
    private final ArrayList<ObserverBoard> observers = new ArrayList<>();

    public Board(int dimension) {
        changeBoardDimension(dimension);//Este ejecuta addLights()
    }

    public int getDimension() {
        return dimension;
    }

    private void addLights() {
        lights = new Light[getDimension()][getDimension()];
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                int rand = Utils.getRandomBetween(2,10);
                if (i % 2 == 0 && (j + rand) % 2 != 0) {
                    lights[i][j] = new Light(i, j, true);
                } else {
                    lights[i][j] = new Light(i, j, false);
                }
            }
        }
    }

    public void changeBoardDimension(int dimension) {
        this.dimension = dimension;
        addLights();
        notifyAllBoardDimensionObservers();
    }

    public void resetBoard() {
        addLights();
        notifyAllResetBoardObservers();
    }

    public void processSwitchLight(Light light) {
        boolean switchUp = (light.getCol() - 1) < dimension && (light.getCol() - 1) >= 0;
        boolean switchDown = (light.getCol() + 1) < dimension && (light.getCol() + 1) >= 0;
        boolean switchLeft = (light.getRow() - 1) < dimension && (light.getRow() - 1) >= 0;
        boolean switchRight = (light.getRow() + 1) < dimension && (light.getRow() + 1) >= 0;

        lights[light.getCol()][light.getRow()].switchOn();
        if (switchUp) lights[light.getCol() - 1][light.getRow()].switchOn();
        if (switchDown) lights[light.getCol() + 1][light.getRow()].switchOn();
        if (switchLeft) lights[light.getCol()][light.getRow() - 1].switchOn();
        if (switchRight) lights[light.getCol()][light.getRow() + 1].switchOn();
        notifyAllLightsSwitchObservers();
        verifyWins();
    }

    public void processChangeLevel(Integer level) {
        switch (level) {
            case 1:
                changeBoardDimension(4);
                break;
            case 2:
                changeBoardDimension(5);
                break;
            case 3:
                changeBoardDimension(6);
                break;
            case 4:
                changeBoardDimension(7);
                break;
            case 5:
                changeBoardDimension(8);
                break;
            default:
                throw new IllegalArgumentException("Nivel invalido. Validos : 1 - 5. Recibido : " + level);
        }

    }

    private void verifyWins() {
        boolean allLightsOff = true;
        for (Light[] lightList : lights) {
            for (Light light : lightList) {
                if (light.isOn()) {
                    allLightsOff = false;
                    break;
                }
            }
        }
        if(allLightsOff) {
            notifyAllBoardWins();
        }
    }

    public Boolean[][] getLightsStatus() {
        Boolean[][] lightsStatus = new Boolean[dimension][dimension];
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                lightsStatus[i][j] = lights[i][j].isOn();
            }
        }
        return lightsStatus;
    }

    public void attach(ObserverBoard observer) {
        observers.add(observer);
    }

    private void notifyAllLightsSwitchObservers() {
        for (ObserverBoard observer : observers) {
            observer.updateLights();
        }
    }

    private void notifyAllBoardDimensionObservers() {
        for (ObserverBoard observer : observers) {
            observer.updateBoardDimension();
        }
    }

    private void notifyAllBoardWins() {
        for (ObserverBoard observer : observers) {
            observer.updateGameWinNotification();
        }
    }

    private void notifyAllResetBoardObservers() {
        for (ObserverBoard observer : observers) {
            observer.updateResetBoard();
        }
    }

}

package model;

public interface ObserverGameData {
    void updateMovements();
    void resetMovements();
}

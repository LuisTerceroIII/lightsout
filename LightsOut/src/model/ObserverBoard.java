package model;

public interface ObserverBoard {
    void updateLights();
    void updateBoardDimension();
    void updateGameWinNotification();
    void updateResetBoard();
}

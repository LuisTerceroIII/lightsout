package model;

public class LightsOut {
	private final Board board;
	private final GameData gameData;

	public LightsOut(Board board, GameData gameData) {
		this.board = board;
		this.gameData = gameData;
	}
	public Board getBoard() {
		return board;
	}

	public GameData getGameData() {
		return gameData;
	}

	public void processSwitchLight(Light light) {
		getBoard().processSwitchLight(light);
	}

	public void plusMovement() {
		getGameData().plusMovement();
	}

	public void processChangeLevel(Integer level) {
		getBoard().processChangeLevel(level);
	}

	public void resetMovements() {
		getGameData().resetMovements();
	}

    public void resetBoard() {
		getBoard().resetBoard();
    }

	public int getBoardDimension() {
		return getBoard().getDimension();
	}

	public Boolean[][] getLightsStatus() {
		return getBoard().getLightsStatus();
	}
}

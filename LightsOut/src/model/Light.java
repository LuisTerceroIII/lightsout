package model;

public class Light {
    private int col;
    private int row;
    private boolean on;

    public Light(int col, int row, boolean on) {
        super();
        this.col = col;
        this.row = row;
        this.on = on;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public void switchOn() {
        this.on = !on;
    }
}

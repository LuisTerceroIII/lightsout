package view;

import javax.swing.JButton;
import java.awt.Color;

public class LightView {
	private JButton button;
	private int col;
	private int row;
	private boolean on;
	
	public LightView(int col, int row, boolean on) {
		this.col = col;
		this.row = row;
		button = new JButton("");
		setOn(on);
	}

	public JButton getButton() {
		return button;
	}

	public void setButton(JButton button) {
		this.button = button;
	}

	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		if(on) {
			button.setBackground(new Color(255,255,0));
		} else {
			button.setBackground(new Color(255,255,255));
		}
		button.revalidate();
		this.on = on;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

}

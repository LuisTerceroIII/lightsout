package view;

import javax.swing.*;
import java.awt.*;

public class MovementsCounterView {
    private final JLabel movementsLabel;

    public MovementsCounterView() {
        movementsLabel = new JLabel(0+"");
        movementsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        movementsLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
        movementsLabel.setBounds(31, 154, 171, 40);
    }

    public JLabel getMovements() {
        return movementsLabel;
    }

    public void plusMovement() {
        int movementsCounter = Integer.parseInt(this.movementsLabel.getText());
        movementsCounter++;
        getMovements().setText(movementsCounter + "");
    }

    public void resetMovements() {
        getMovements().setText(0 + "");
    }
}

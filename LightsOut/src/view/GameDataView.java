package view;

import controller.LightsOutController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameDataView {
    private final JPanel panel;
    private final MovementsCounterView movements;
    private final LevelsView levelSelector;
    private LightsOutController controller;

    public GameDataView(Integer[] levels) {
        this.movements = new MovementsCounterView();
        this.levelSelector = new LevelsView(levels);

        //Aca se setea el panel principal
        panel = new JPanel();
        panel.setForeground(Color.GRAY);
        panel.setBackground(Color.WHITE);
        panel.setBounds(695, 89, 237, 341);
        panel.setLayout(null);

        //Aca se agrega el "componente" de los niveles. Label + comboBox
        panel.add(getLevelLabel());
        panel.add(levelSelector.getComboBox());

        //Aca se agrega el "componente" de los movimientos. Label + label
        panel.add(getMovementLabel());
        panel.add(movements.getMovements());

        //Aca se agrega el button de restart. Button
        panel.add(getRestartButton());

    }

    public JPanel getPanel() {
        return panel;
    }

    public MovementsCounterView getMovements() {
        return movements;
    }

    public LevelsView getLevelSelector() {
        return levelSelector;
    }

    public void setController(LightsOutController controller) {
        this.controller = controller;
        this.levelSelector.setController(controller);
    }

    private JButton getRestartButton() {
        JButton restart = new JButton("RESTART");
        restart.setBackground(Color.WHITE);
        restart.setForeground(Color.BLACK);
        restart.setFont(new Font("Tahoma", Font.PLAIN, 23));
        restart.setBounds(31, 217, 171, 78);
        restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.resetBoard();
            }
        });
        return restart;
    }

    private JLabel getMovementLabel() {
        JLabel movementsLabel = new JLabel("MOVEMENTS");
        movementsLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        movementsLabel.setBounds(31, 129, 171, 14);
        return movementsLabel;
    }

    private JLabel getLevelLabel() {
        JLabel levelLabel = new JLabel("LEVELS");
        levelLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        levelLabel.setBounds(31, 33, 171, 14);
        return levelLabel;
    }

    public void plusMovement() {
        getMovements().plusMovement();
    }

    public void resetMovements() {
        getMovements().resetMovements();
    }

}

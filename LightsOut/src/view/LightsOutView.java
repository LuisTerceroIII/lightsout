package view;

import java.awt.*;

import javax.swing.*;

import controller.LightsOutController;
import model.Light;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LightsOutView {

    private JFrame frame;
    private BoardView board;
    private GameDataView gameData;

    public LightsOutView(BoardView board, GameDataView gameData) {
        this.board = board;
        this.gameData = gameData;
        initialize();
    }

    /**
     * Launch the application.
     */
    public void start() {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LightsOutView window = new LightsOutView(board, gameData);
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initialize() {
        frame = new JFrame();
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(100, 100, 981, 658);
        frame.getContentPane().add(board.getBoard());
        frame.getContentPane().add(gameData.getPanel());

        JLabel title = new JLabel("LIGHTS OUT");
        title.setFont(new Font("Tahoma", Font.PLAIN, 59));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setBounds(129, 11, 679, 57);
        frame.getContentPane().add(title);
    }

    public BoardView getBoard() {
        return board;
    }

    private void setBoard(BoardView board) {
        this.board = board;
    }

    public GameDataView getGameData() {
        return gameData;
    }

    public void setGameData(GameDataView gameData) {
        this.gameData = gameData;
    }

    //Setea el controlador que todos los componentes que cambien el estado de la aplicacion van a usar!
    public void setController(LightsOutController controller) {
        getBoard().setController(controller);
        getGameData().setController(controller);
    }

    public void updateLights(Boolean[][] lightsStatus) {
        getBoard().setLightsStatus(lightsStatus);
    }

    public void updateBoardDimension(int dimension,Boolean[][] lightsStatus) {
        getBoard().setDimension(dimension,lightsStatus);
    }

    public void updateWins() {
        ImageIcon trophy = new ImageIcon("src/images/trophy.png");
        JOptionPane.showMessageDialog(null, "¡Haz ganado!",
                "Lights Out", JOptionPane.INFORMATION_MESSAGE, trophy);
    }

    public void updateMovement() {
        getGameData().plusMovement();
    }

    public void resetMovements() {
        getGameData().resetMovements();
    }

    public void resetBoard(Boolean[][] lightsStatus) {
        getBoard().setLightsStatus(lightsStatus);
    }
}

package view;

import controller.LightsOutController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LevelsView {

    private final JComboBox<Integer> comboBox;
    private LightsOutController controller;

    public LevelsView(Integer[] levels) {
        this.comboBox = new JComboBox<Integer>(levels);
        comboBox.setForeground(Color.BLACK);
        comboBox.setBounds(31, 64, 171, 40);

        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox<Integer> comboBox = (JComboBox<Integer>) e.getSource();
                Integer level = (Integer) comboBox.getSelectedItem();
                controller.processChangeLevel(level);
            }
        });
    }

    public JComboBox<Integer> getComboBox() {
        return comboBox;
    }

    public void setController(LightsOutController controller) {
        this.controller = controller;
    }
}

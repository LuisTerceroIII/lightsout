package view;

import controller.LightsOutController;
import model.Light;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BoardView {
    private LightView[][] lights;
    private Boolean[][] lightsStatus;
    private int dimension;
    private final JPanel panel;
    private LightsOutController controller;

    /*Recibe una matriz de booleanos, los true significan luz prendida y falso apagada.*/
    public BoardView(int dimension, Boolean[][] lightsStatus) {
        this.dimension = dimension;
        this.lights = new LightView[dimension][dimension];
        this.lightsStatus = lightsStatus;
        this.panel = panelConfig(dimension);
        paintLights();
    }

    private JPanel panelConfig(int dimension) {
        JPanel panel = new JPanel();
        panel.setBackground(Color.BLACK);
        panel.setBounds(33, 89, 626, 480);
        panel.setLayout(new GridLayout(dimension, dimension, 15, 15));
        return panel;
    }

    public void setDimension(int dimension, Boolean[][] lightsStatus) {
        this.dimension = dimension;
        this.panel.removeAll();
        panel.setLayout(new GridLayout(dimension, dimension, 15, 15));
        this.lights = new LightView[dimension][dimension];
        this.lightsStatus = lightsStatus;
        this.panel.updateUI();
        paintLights();
    }
    public int getDimension() {
        return dimension;
    }

    public JPanel getBoard() {
        return panel;
    }

    public void setController(LightsOutController controller) {
        this.controller = controller;
    }

    private void paintLights() {
        for(int i = 0; i< lights.length ; i++) {
            for(int j = 0; j < lights[i].length; j++) {
                boolean isOn = lightsStatus[i][j];
                lights[i][j] = new LightView(i,j,isOn);
                LightView light = lights[i][j];
                addLight(light);
                light.getButton().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        controller.plusMovement();
                        controller.processSwitch(light.getCol(),light.getRow(),light.isOn());
                    }
                });
            }
        }
    }

    private void addLight(LightView lightView) {
        panel.add(lightView.getButton());
    }

    public void setLightsStatus(Boolean[][] lightsStatus) {
        for (int i = 0; i < lights.length; i++) {
            for (int j = 0; j < lights[i].length; j++) {
                lights[i][j].setOn(lightsStatus[i][j]);
            }
        }
    }
}

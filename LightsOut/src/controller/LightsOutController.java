package controller;

import model.Light;
import model.LightsOut;
import model.ObserverBoard;
import model.ObserverGameData;
import view.LightsOutView;

public class LightsOutController implements ObserverBoard, ObserverGameData {
    private final LightsOut lightsOut;
    private final LightsOutView lightsOutView;

    public LightsOutController(LightsOut lightsOut, LightsOutView lightsOutView) {
        this.lightsOut = lightsOut;
        this.lightsOutView = lightsOutView;
        this.lightsOut.getBoard().attach(this); //Agregamos el controlador al arrayList de observers de Board (Model)
        this.lightsOut.getGameData().attach(this);//Agregamos el controlador al arrayList de observers de GameData (Model)
    }
    /*Comienza la interfaz*/
    public void launchApp() {
        lightsOutView.start();
    }

    /*Este llamado provoca que se ejecute el updateBoard del observer de Board. En ese metodo "updateBoard" se hace el llamado a lightsOutView para que actulice la interfaz.**/
    public void processSwitch(int col, int row, boolean on) {
        Light light = new Light(col,row,on);
        lightsOut.processSwitchLight(light);
    }

    public void plusMovement() {
        lightsOut.plusMovement();
    }

    public void processChangeLevel(Integer level) {
        lightsOut.processChangeLevel(level);
        lightsOut.resetMovements();
    }

    public void resetBoard() {
        lightsOut.resetBoard();
        this.resetMovements();
    }

    @Override
    public void updateLights() {
        lightsOutView.updateLights(lightsOut.getBoard().getLightsStatus()); // Enviamos matriz de booleanos actualizada para que se refrescar el tablero
    }

    @Override
    public void updateBoardDimension() {
        lightsOutView.updateBoardDimension(lightsOut.getBoardDimension(),lightsOut.getLightsStatus());
    }

    @Override
    public void updateGameWinNotification() {
        lightsOutView.updateWins();
        resetBoard();
    }

    @Override
    public void updateResetBoard() {
        lightsOutView.resetBoard(lightsOut.getLightsStatus());
    }

    @Override
    public void updateMovements() {
        lightsOutView.updateMovement();
    }

    @Override
    public void resetMovements() {
        lightsOutView.resetMovements();
    }

}

import controller.LightsOutController;
import model.Board;
import model.GameData;
import model.LightsOut;
import view.BoardView;
import view.GameDataView;
import view.LightsOutView;

public class MAIN {
    /*
     * Se inician todos los objetos necesarios y se construye el controlador.
     * El controlador inicia la app.
     * */
    public static void main(String[] args) {
        int dimension = 4;
        Integer[] levels = {1, 2, 3, 4, 5};

        Board board = new Board(dimension);
        GameData gameData = new GameData(levels);
        LightsOut lightsOut = new LightsOut(board, gameData);
        BoardView boardView = new BoardView(dimension, board.getLightsStatus()); //Envio matriz de booleanos para organizar las luces.
        GameDataView gameDataView = new GameDataView(levels);
        LightsOutView lightsOutView = new LightsOutView(boardView, gameDataView);

        LightsOutController controller = new LightsOutController(lightsOut, lightsOutView);

        //Comparto controlador con vista para que esta puede enviarle la data que ingresa el usuario.
        lightsOutView.setController(controller);
        controller.launchApp();
    }
}
